Name:           spirv-cross
Version:        1.4.304
Release:        1
Summary:        SPIRV-Cross is a practical tool and library for performing reflection on SPIR-V and disassembling SPIR-V back to high-level languages.

License:        Apache 2.0
URL:            https://github.com/KhronosGroup/SPIRV-Cross
Source0:        https://github.com/KhronosGroup/SPIRV-Cross/archive/refs/tags/vulkan-sdk-%{version}.%{release}.tar.gz

BuildRequires:  cmake
BuildRequires:  gcc-c++
BuildRequires:  make
BuildRequires:  python3

# Disable debug source package to avoid issues with empty debugsourcefiles.list
%define debug_package %{nil}

%description
SPIRV-Cross is a tool and library for parsing and converting SPIR-V to other shader languages.
It is commonly used in graphics development to convert SPIR-V bytecode into GLSL, HLSL, Metal Shading Language, and more.

%prep
%setup -q -n SPIRV-Cross-vulkan-sdk-%{version}.%{release}

%build
mkdir -p build
cd build
# Use RelWithDebInfo to include debug symbols in the binaries
cmake .. -DCMAKE_INSTALL_PREFIX=%{_prefix} -DCMAKE_BUILD_TYPE=Release
make %{?_smp_mflags}

%install
cd build
make install DESTDIR=%{buildroot}

%files
%{_bindir}/spirv-cross
%{_libdir}/libspirv-cross-c.a
%{_libdir}/libspirv-cross-core.a
%{_libdir}/libspirv-cross-cpp.a
%{_libdir}/libspirv-cross-glsl.a
%{_libdir}/libspirv-cross-hlsl.a
%{_libdir}/libspirv-cross-msl.a
%{_libdir}/libspirv-cross-reflect.a
%{_libdir}/libspirv-cross-util.a
%{_libdir}/pkgconfig/spirv-cross-c.pc
%{_includedir}/spirv_cross/*
%{_datadir}/spirv_cross_c/cmake/*
%{_datadir}/spirv_cross_core/cmake/*
%{_datadir}/spirv_cross_cpp/cmake/*
%{_datadir}/spirv_cross_glsl/cmake/*
%{_datadir}/spirv_cross_hlsl/cmake/*
%{_datadir}/spirv_cross_msl/cmake/*
%{_datadir}/spirv_cross_reflect/cmake/*
%{_datadir}/spirv_cross_util/cmake/*

%changelog
* Thu Oct 05 2023 Your Name <saypaul@redhat.com> - 0.0.1-1
- Initial package for SPIRV-Cross.